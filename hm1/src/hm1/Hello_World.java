package hm1;

public class Hello_World {
	public static final String HELLO_WORLD= "Hello World";
		
		public static void main(String[] args) {
			System.out.println(HELLO_WORLD);
			System.out.println(helloWorldStatic());
			
			Hello_World hw = new Hello_World();
			System.out.println(hw.helloWorldNonStatic());
		}
	
		private static String helloWorldStatic(){
			return HELLO_WORLD;
		}
	
		private String helloWorldNonStatic(){
			return HELLO_WORLD;
		}

		
}
