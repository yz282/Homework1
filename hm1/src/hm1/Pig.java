package hm1;

public class Pig extends Pet{

	public Pig(String name) {
		super(name);
	}

	@Override
	public void makeNoise() {
		System.out.println("Oink");
		
	}

}
